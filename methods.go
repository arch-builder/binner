package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/url"
	"os"
	"path/filepath"
	"time"
)

func (b *Builder) Status(user, buildid string) *Status {
	s, ok := b.builds[user+"_"+buildid]
	if ok {
		if s.User != user {
			return &Status{
				Message: "user is the not the owner of the build",
			}
		}
		return s
	}

	bjson, err := os.Open(filepath.Join("/var/log/binner", user, buildid, "build.json"))
	if os.IsNotExist(err) {
		return &Status{
			Message: "Build not found",
		}
	}
	if err != nil {
		return &Status{
			Message: fmt.Sprintf("Error : some other error : %s, contact the admin.", err),
		}
	}
	defer bjson.Close()

	content, err := ioutil.ReadAll(bjson)
	if err != nil {
		return &Status{
			Message: fmt.Sprintf("Error : some other error : %s, contact the admin.", err),
		}
	}

	var reply Status

	err = json.Unmarshal(content, &reply)
	if err != nil {
		return &Status{
			Message: fmt.Sprintf("Error : some other error : %s, contact the admin.", err),
		}
	}

	if reply.User != user {
		return &Status{
			Message: "Build not found",
		}
	}

	return &reply
}

type ListReply struct {
	List    []string `json:"list"`
	Message string   `json:"message"`
	IsError bool     `json:"is_error"`
}

func (b *Builder) ListBuilds(user string) *ListReply {
	list := make([]string, 0)
	dirstats, err := ioutil.ReadDir(filepath.Join("/var/log/binner", user))
	if err != nil {
		if os.IsNotExist(err) {
			return &ListReply{
				List:    list,
				Message: "No builds yet",
				IsError: true,
			}
		}
		return &ListReply{
			List:    list,
			Message: fmt.Sprintf("Error : some other error : %s, contact the admin.", err),
			IsError: true,
		}
	}

	for _, stat := range dirstats {
		if stat.IsDir() {
			list = append(list, stat.Name())
		}
	}

	return &ListReply{
		List:    list,
		Message: "success",
		IsError: false,
	}
}

func (b *Builder) Build(user, aurRepo string) *Status {
	buildId := fmt.Sprintf("build%d", rand.Int63())

	fmt.Println("Build Started : ", buildId)

	status := &Status{Completed: false, Success: false, BuildID: buildId}
	b.builds[user+"_"+buildId] = status
	status.Success = false

	status.Start = time.Now().Unix()
	if user == "" {
		status.end("Build failed : No username given")
		return status
	}
	if aurRepo == "" {
		status.end("No AUR git repo URL given")
		return status
	}

	status.User = user
	u, err := url.Parse(aurRepo)
	if err != nil {
		status.end(fmt.Sprintf("URL parse error : %s", err))
		return status
	}

	status.URL = u

	go b.buildArtifact(status)
	return status
}
