package main

import (
	"flag"
	"fmt"
	"os"
)

var logerr = func(a ...interface{}) {
	fmt.Fprintln(os.Stderr, a...)
}

var logmsg = func(a ...interface{}) {
	fmt.Println(a...)
}

func main() {
	rootfs := flag.String("b", "", "location of the arch base rootfs")
	repo := flag.String("r", "", "location of the repository directory")
	runMode := flag.Bool("m", false, "run binner in master mode. [See docs for more... Hopefully]")
	containerName := flag.String("n", "", "name of the container")

	flag.Parse()

	if *rootfs == "" {
		logerr("CommandLine::Parse : rootfs not specified")
		os.Exit(200)
	}

	if *runMode {
		if *repo == "" {
			logerr("Commandline::Parse : repo root not specified")
			os.Exit(200)
		}
		logmsg("CommandLine::Parse : running in master mode")
		if *containerName != "" {
			logmsg("Warning : cannot pass container name to the master process. Ignoring.")
		}
		RunMaster(*rootfs, *repo)
	} else {
		logmsg("CommandLine::Parse : running in builder mode")
		RunContainer(*rootfs, *containerName)
	}

}

func RunContainer(rootfs, containerName string) {
	c, err := NewContainer(rootfs, containerName)
	if err != nil {
		logerr(err)
		os.Exit(200)
	}
	err = c.RunBuildProcess()
	if err != nil {
		logerr(err)
		os.Exit(200)
	}
}

func RunMaster(rootfs, repo string) {
	b := NewBuilder(rootfs, repo)
	if err := b.run(); err != nil {
		logerr(err)
		os.Exit(200)
	}
}
