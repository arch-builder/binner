//

/*
base                          : base root fs which will be mounted as ro in the overlay
$USERDIR/$PROJECTDIR/rw       : the overlay rw fs where the package is built and output
$USERDIR/$PROJECTDIR/w        : no idea what this does....
$USERDIR/$PROJECTDIR/squashed : the final resulting fs where the work is done
*/
package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	git "gopkg.in/src-d/go-git.v4"
)

type Builder struct {
	repoRoot    string
	rootfs      string
	commChannel net.Listener
	builds      map[string]*Status
}

type Status struct {
	User      string     `json:"user"`
	BuildID   string     `json:"build_id"`
	URL       *url.URL   `json:"url"`
	Start     int64      `json:"start_timestamp"`
	End       int64      `json:"end_timestamp"`
	Completed bool       `json:"completed"`
	Success   bool       `json:"success"`
	Message   string     `json:"message"`
	Packages  []*PkgInfo `json:"packages,omitempty"`
	stdout    *os.File
	stderr    *os.File
}

func (s *Status) end(message string) {
	s.Message = message
	s.Completed = true
	s.Success = false
	s.End = time.Now().Unix()
}

func NewBuilder(rootfs, repoRoot string) *Builder {
	return &Builder{rootfs: rootfs, repoRoot: repoRoot, builds: make(map[string]*Status)}
}

func (b *Builder) run() error {
	err := os.MkdirAll("/var/log/binner/", 0666)
	if err != nil {
		return fmt.Errorf("Unable to create log directory (/var/log/binner/) : %s", err)
	}

	b.commChannel, err = net.Listen("unix", "/tmp/binner.sock")
	if err != nil {
		return err
	}
	defer b.commChannel.Close()

	rand.Seed(time.Now().Unix())

	for {
		conn, err := b.commChannel.Accept()
		if err != nil {
			continue
		}

		go b.handle(conn)
	}
}

type JsonCall struct {
	Method string   `json:"method"`
	Args   []string `json:"args"`
}

type Reply struct {
	Method string      `json:"method"`
	Reply  string      `json:"reply"`
	Data   interface{} `json:"data"`
}

func (b *Builder) handle(conn net.Conn) {
	dec := json.NewDecoder(conn)
	enc := json.NewEncoder(conn)
	for {
		call := JsonCall{}
		err := dec.Decode(&call)
		if err != nil {
			return
		}
		var reply = &Reply{
			Method: call.Method,
		}
		switch call.Method {
		case "build":
			if len(call.Args) != 2 {
				reply.Reply = "error"
				reply.Data = "wrong number of arguments passed to the method"
				enc.Encode(reply)
				continue
			}
			reply.Data = "data"
			reply.Data = b.Build(call.Args[0], call.Args[1])
		case "status":
			if len(call.Args) != 2 {
				reply.Reply = "error"
				reply.Data = "wrong number of arguments passed to the method"
				enc.Encode(reply)
				continue
			}
			reply.Data = "data"
			reply.Data = b.Status(call.Args[0], call.Args[1])
		case "list_builds":
			if len(call.Args) != 1 {
				reply.Reply = "error"
				reply.Data = "wrong number of arguments passed to the method"
				enc.Encode(reply)
				continue
			}
			reply.Data = "data"
			reply.Data = b.ListBuilds(call.Args[0])
		}
		err = enc.Encode(reply)
		if err != nil {
			return
		}
	}
}

func (b *Builder) buildArtifact(status *Status) {
	u := status.URL

	project := filepath.Base(u.Path)
	logpath := filepath.Join("/var/log/binner/", status.User, status.BuildID)
	defer func() {
		f, err := os.Create(filepath.Join(logpath, "build.json"))
		if err != nil {
			logerr(err)
			return
		}
		enc := json.NewEncoder(f)
		err = enc.Encode(&status)
		if err != nil {
			logerr(err)
			return
		}
		f.Close()
	}()

	err := os.MkdirAll(logpath, 0755)
	if err != nil {
		status.end(fmt.Sprintf("Error while creating log directories : %s. Contact the Admin", err))
		return
	}

	if file, err := os.Create(filepath.Join(logpath, "stdout")); err != nil {
		status.end(fmt.Sprintf("Error while creating log files : %s. Contact the Admin", err))
		return
	} else {
		status.stdout = file
	}

	if file, err := os.Create(filepath.Join(logpath, "stderr")); err != nil {
		status.end(fmt.Sprintf("Error while creating log files : %s. Contact the Admin", err))
		return
	} else {
		status.stderr = file
	}

	defer func() {
		status.stderr.Close()
		status.stdout.Close()
	}()

	userdir := filepath.Join("/tmp", "binner", status.User)
	userproject := filepath.Join(userdir, project)

	status.Message = "Setting up build environment"
	// Create directories
	err = b.createDirectories(userproject)
	if err != nil {
		status.end("Error while creating build directories. Contact the Admin")
		return
	}
	defer os.RemoveAll(userproject)

	// Clone the AUR Repo
	_, err = git.PlainClone(filepath.Join(userproject, "rw", "build"), false, &git.CloneOptions{
		URL: status.URL.String(),
	})
	if err != nil {
		status.end(fmt.Sprintf("Error while cloning the AUR repo : %s", err))
		return
	}

	if err := chownAll(filepath.Join(userproject, "rw", "build"), 1000, 1000); err != nil {
		status.end(fmt.Sprintf("Error while setting up permissions : %s, contact the admin", err))
		return
	}

	squashed := filepath.Join(userproject, "squashed")
	rwdir := filepath.Join(userproject, "rw")
	wdir := filepath.Join(userproject, "w")

	// Setup overlay mount
	err = syscall.Mount(status.User+"_"+project, squashed, "overlay", 0, "lowerdir="+b.rootfs+",upperdir="+rwdir+",workdir="+wdir)
	if err != nil {
		status.end(fmt.Sprintf("Error while mounting the overlayfs : %s, contact the admin", err))
		return
	}
	defer syscall.Unmount(squashed, 0)

	status.Message = "Spawning a new container..."
	cmd := exec.Command(os.Args[0], "-b", squashed, "-n", status.User+"_"+project)
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags: syscall.CLONE_NEWUTS |
			syscall.CLONE_NEWPID |
			syscall.CLONE_NEWIPC |
			syscall.CLONE_NEWNS,
	}

	cmd.Stdout = status.stdout
	cmd.Stderr = status.stderr
	status.Message = "Running the build..."
	err = cmd.Run()
	if err != nil {
		status.end(fmt.Sprintf("Error while running the build container : %s, contact the admin", err))
		return
	}

	status.Message = "Build done... Transferring the artifact to the user repo."

	pkgdir := filepath.Join(rwdir, "build", "pkg")

	pkgs, err := ioutil.ReadDir(pkgdir)
	if err != nil {
		status.end(fmt.Sprintf("Error while transferring the artifact : %s, contact the admin", err))
		return
	}

	// Create the binary repo if not present
	userRepoRoot := filepath.Join(b.repoRoot, status.User)
	err = os.MkdirAll(userRepoRoot, 0755)
	if err != nil {
		status.end(fmt.Sprintf("Error while transferring the artifact : %s, contact the admin", err))
		return
	}

	for _, pkg := range pkgs {
		if !pkg.IsDir() {
			continue
		}
		content, err := ioutil.ReadFile(filepath.Join(pkgdir, pkg.Name(), ".PKGINFO"))
		if err != nil {
			status.Message = fmt.Sprintf("Error while transferring the artifact %s : %s, contact the admin",
				pkg.Name(), err)
			continue
		}

		pinfo := parsePKGINFO(content)

		pkgname := pinfo.Pkgname + "-" + pinfo.Pkgver + "-" + pinfo.Arch + ".pkg.tar.xz"

		err = copyFile(filepath.Join(rwdir, "build", pkgname), filepath.Join(userRepoRoot, pkgname))
		if err != nil {
			status.Message = fmt.Sprintf("Error while transferring the artifact %s : %s, contact the admin",
				pkg.Name(), err)
		}

		status.Packages = append(status.Packages, pinfo)
	}

	if err := refreshDB(userRepoRoot); err != nil {
		status.end(fmt.Sprintf("Error refreshing the db of the user : %s, contact the admin", err))
		return
	}
	status.Completed = true
	status.Success = true
	status.End = time.Now().Unix()
	status.Message = "Success"
}

func refreshDB(reporoot string) error {

	user := filepath.Base(reporoot)

	files := make([]string, 0)
	files = append(files, "-R", filepath.Join(reporoot, user+".db.tar.gz"))
	dirInfo, err := ioutil.ReadDir(reporoot)
	if err != nil {
		return err
	}

	for _, i := range dirInfo {
		if i.IsDir() {
			continue
		}
		if strings.HasSuffix(i.Name(), ".pkg.tar.xz") {
			files = append(files, filepath.Join(reporoot, i.Name()))
		}
	}

	cmd := exec.Command("repo-add", files...)
	return cmd.Run()
}

func copyFile(src, dst string) error {
	out, err := os.Open(src)
	if err != nil {
		return err
	}
	file, err := os.Create(dst)
	if err != nil {
		return err
	}
	_, err = io.Copy(file, out)
	if err != nil && err != io.EOF {
		return err
	}
	file.Close()
	out.Close()
	return nil
}

func (b *Builder) createDirectories(userproject string) error {
	err := os.MkdirAll(userproject, 0755)
	if err != nil {
		return err
	}
	err = os.MkdirAll(filepath.Join(userproject, "rw", "build"), 0755)
	if err != nil {
		return err
	}
	err = os.MkdirAll(filepath.Join(userproject, "w"), 0755)
	if err != nil {
		return err
	}
	return os.MkdirAll(filepath.Join(userproject, "squashed"), 0755)
}

func chownAll(dir string, uid int, gid int) error {
	return filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		return os.Chown(path, uid, gid)
	})
}
