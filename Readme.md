# binner

binner is a simple (experimental) build manager for arch linux packages.

## What it does?
It has two modes, namely master mode and container mode.

#### In `master` mode
 + It listens at a domain socket (`/tmp/binner.sock`), for any new connections
 + This receives commands (json objects) from the connections and runs them accordingly.
 + Folowing commands are added so far
   - `build` : builds the given url for a user
               (url is the git repo containing the PKGBUILD)
      ```json
      {"method" : "build", "args": [ "username", "http://some.url/project.git" ]}
      ```
      Returns a Status object wrapped in Reply object as a response.
   - `list_builds` : list lists all the build's ID of a specific user.
      ```json
      {"method" : "list_builds", "args": [ "username" ]}
      ```
      Returns a ListReply object wrapped in Reply object as a response.
   - `status` : status returns the status of the build
      ```json
      {"method" : "status", "args": [ "username", "someBuildId" ]}
      ```
 + `build` command spins up a container with an arch rootfs. (Overlay FS)
   - RO layer : an arch.img mounted to a directory with `pacstrap`ped with `base`, `base-devel`, `git`
   - RW layer : an initial empty directory with build sub directory where all the build takes place.
     ie., the git repo specified above is cloned at build sub directory.

   Another instance of binner is run in container mode with this overlay fs directory as the rootfs.

#### In `container` mode
 + First all the needed directories and `/dev/` nodes are set up.
 + Isolated `proc` is mounted.
 + Also the stderr and stdout are pointed to regular files in the original filesystem by the master process.
 + Chrooted into the rootfs directory (All the namespaces for this process is set up in the master `process`)
 + `pacman -Syu --noconfirm --force` // Have to replace --force with --overwrite
 + `cd /build && makepkg -s --noconfirm` // Also all the needed users, permissions and visudo are setup in the arch.img (base) beforehand.
 + Fails??? => exit with a non-zero exit code.
 + Passed??? => awesome!!!

### Post container build step
 + `master` reads the .PKGINFO for package details and moves all packages in the `/build/pkg/**/*pkg.tar.xz` to the binary repo directory under the user's directory (`-repo`)
 + Writes the build info at `/var/log/binner/user/buildID/build.json` where the logs of the build also reside.


## Usage
```shell
# Needs root access
binner -m -b /path/to/arch/base/image/directory/ -r /path/to/package/storage/repo/
```

 + `-m` : run in master mode
 + `-b` : Path to the arch.img mount directory (arch linux root fs)
 + `-r` : Path to the directories where the binaries packages are to be stored.