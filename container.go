package main

import (
	"errors"
	"os"
	"os/exec"
	"syscall"
)

type NodeInfo struct {
	Dev  uint64
	Mode uint32
}

func Mkdev(major, minor uint32) uint64 {
	dev := (uint64(major) & 0x00000fff) << 8
	dev |= (uint64(major) & 0xfffff000) << 32
	dev |= (uint64(minor) & 0x000000ff) << 0
	dev |= (uint64(minor) & 0xffffff00) << 12
	return dev
}

var deviceNodes map[string]NodeInfo = map[string]NodeInfo{
	"/dev/console": NodeInfo{Mode: syscall.S_IFCHR | 0600, Dev: Mkdev(5, 1)},
	"/dev/null":    NodeInfo{Mode: syscall.S_IFCHR | 0666, Dev: Mkdev(1, 3)},
	"/dev/ptmx":    NodeInfo{Mode: syscall.S_IFCHR | 0666, Dev: Mkdev(5, 2)},
	"/dev/random":  NodeInfo{Mode: syscall.S_IFCHR | 0666, Dev: Mkdev(1, 8)},
	"/dev/tty":     NodeInfo{Mode: syscall.S_IFCHR | 0666, Dev: Mkdev(5, 0)},
	"/dev/urandom": NodeInfo{Mode: syscall.S_IFCHR | 0666, Dev: Mkdev(1, 9)},
	"/dev/zero":    NodeInfo{Mode: syscall.S_IFCHR | 0666, Dev: Mkdev(1, 5)},
}

type Container struct {
	rootfs string
	name   string
}

func NewContainer(rootfs, name string) (*Container, error) {
	if rootfs == "" {
		return nil, errors.New("cannot be an empty rootfs")
	}
	if name == "" {
		name = "builder"
	}

	return &Container{rootfs: rootfs, name: name}, nil
}

func (c *Container) setupContainer() error {
	logmsg("Log::Msg : setting hostname")
	if err := syscall.Sethostname([]byte(c.name)); err != nil {
		return err
	}

	logmsg("Log::Msg : chrooting")
	if err := syscall.Chroot(c.rootfs); err != nil {
		return err
	}

	logmsg("Log::Msg : chdir to /")
	if err := os.Chdir("/"); err != nil {
		return err
	}

	oldmask := syscall.Umask(0)

	for devpath, info := range deviceNodes {
		logmsg("Log::Msg : creating node : ", int(info.Dev), devpath)
		if err := syscall.Mknod(devpath, info.Mode, int(info.Dev)); err != nil {
			return err
		}
	}

	syscall.Umask(oldmask)

	logmsg("Log::Msg : mounting proc")
	return syscall.Mount("proc", "proc", "proc", 0, "")
}

func (c *Container) RunBuildProcess() error {
	logmsg("Log::Msg : running as pid : ", os.Getpid())
	// Setup all container stuff for build
	err := c.setupContainer()
	if err != nil {
		return err
	}
	// Cleanup after exit.
	defer func() {
		syscall.Unmount("/proc", 0)
		for devpath, _ := range deviceNodes {
			os.RemoveAll(devpath)
		}
	}()

	os.Chdir("/build/")

	cmd := exec.Command("pacman", "-Syu", "--noconfirm", "--force")
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Run()
	if err != nil {
		return err
	}

	cmd = exec.Command("makepkg", "-s", "--noconfirm")
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	cmd.SysProcAttr = &syscall.SysProcAttr{}

	cmd.SysProcAttr.Credential = &syscall.Credential{
		Uid: 1000,
	}

	logmsg("Log::Msg : exec command")
	return cmd.Run()
}
