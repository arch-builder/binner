package main

import (
	"strconv"
	"strings"
)

type PkgInfo struct {
	Pkgname   string `json:"pkgname,omitempty"`
	Pkgver    string `json:"pkgver,omitempty"`
	Pkgdesc   string `json:"pkgdesc,omitempty"`
	URL       string `json:"pkgurl,omitempty"`
	BuildDate int64  `json:"pkgdate,omitempty"`
	Packager  string `json:"pkger,omitempty"`
	Size      int64  `json:"size,omitempty"`
	Arch      string `json:"arch,omitempty"`
	License   string `json:"license,omitempty"`
}

func parsePKGINFO(content []byte) *PkgInfo {
	c := string(content)

	pinfo := &PkgInfo{}

	lines := strings.Split(c, "\n")
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "#") || line == "" {
			continue
		}

		splits := strings.Split(line, "=")
		if len(splits) < 2 {
			continue
		}

		val := strings.TrimSpace(strings.Join(splits[1:], "="))
		switch strings.TrimSpace(splits[0]) {
		case "pkgname":
			pinfo.Pkgname = val
		case "pkgver":
			pinfo.Pkgver = val
		case "pkgdesc":
			pinfo.Pkgdesc = val
		case "url":
			pinfo.URL = val
		case "builddate":
			num, err := strconv.ParseInt(val, 10, 64)
			if err == nil {
				pinfo.BuildDate = num
			}
		case "packager":
			pinfo.Packager = val
		case "size":
			num, err := strconv.ParseInt(val, 10, 64)
			if err == nil {
				pinfo.Size = num
			}
		case "arch":
			pinfo.Arch = val
		case "license":
			pinfo.License = val
		}
	}
	return pinfo
}
